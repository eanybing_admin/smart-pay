package com.founder.service;

import com.founder.core.domain.RefundOrder;

public interface IRefundOrderService {

    RefundOrder selectRefundOrder(String refundOrderId);

    /**
     * 创建退款订单
     * @param refundOrder
     * @return
     */
    int createRefundOrder(RefundOrder refundOrder);

    /**
     * 更新通知次数
     * @param refundOrderId
     * @param cnt
     * @return
     */
    int updateNotify4Count(String refundOrderId, Integer cnt);
}
